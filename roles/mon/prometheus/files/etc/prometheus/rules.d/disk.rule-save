
ALERT error_on_disk_by_1h
  IF delta(node_filesystem_device_errors_total{mountpoint!~".+docker.+"}[1h]) / delta(node_filesystem_device_errors_total{mountpoint!~".+docker.+"}[2h]) > 0.6
  FOR 1m
  LABELS {
    severity="warning"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} наблюдается возрастание ошибок в точке монтирования {{$labels.mountpoint}}.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT fs_to_readonly_1m
  IF changes(node_filesystem_readonly[1m]) > 0
  LABELS {
    severity="error"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} точка монтирования {{$labels.mountpoint}} перешла в read-only режим, что свидетельствует о возникновении ошибок на уровне FS.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT disk_will_fill_in_4h
  IF predict_linear(node_filesystem_free[1h], 4*3600) < 0
  FOR 5m
  LABELS {
    severity="warning"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} вот вот закончится место (остается примерно на 4 часа работы).\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT disk_fill_90percent
  IF (node_filesystem_free / node_filesystem_size) <= 0.1
  FOR 10s
  LABELS {
    severity="warning"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} осталось менее 10% свободного места.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT disk_fill_95percent
  IF (node_filesystem_free / node_filesystem_size) <= 0.05
  FOR 10s
  LABELS {
    severity="error"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} осталось менее 5% свободного места.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT disk_fill_99percent
  IF (node_filesystem_free / node_filesystem_size) <= 0.01
  FOR 1s
  LABELS {
    severity="critical"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} осталось менее 1% свободного места.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}, создание новых файлов становится невозможным.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT inode_will_fill_in_4h
  IF predict_linear(node_filesystem_files_free[1h], 4*3600) < 0
  LABELS {
    severity="warning"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} вот вот закончатся свободные inode (остается примерно на 4 часа работы).\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT inode_fill_90percent
  IF (node_filesystem_files_free / node_filesystem_files) <= 0.1
  LABELS {
    severity="warning"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} осталось менее 10% свободных inode.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT inode_fill_95percent
  IF (node_filesystem_files_free / node_filesystem_files) <= 0.05
  LABELS {
    severity="error"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} осталось менее 5% свободных inode.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }

ALERT inode_fill_99percent
  IF (node_filesystem_files_free / node_filesystem_files) <= 0.01
  LABELS {
    severity="critical"
  }
  ANNOTATIONS {
    summary = "На ноде {{$labels.instance}} в точке монтирования {{$labels.mountpoint}} осталось менее 1% свободных inode.\nFS тип {{$labels.fstype}}, устройство {{$labels.device}}, создание новых файлов становится невозможным.",
    description = "{{$labels.instance}}/{{$labels.fstype}}/{{$labels.device}}{{$labels.mountpoint}} of job {{$labels.job}} troubles"
  }
