### Пример конфига по умолчанию
```
#---------------------------------------------------------------------
# Example configuration for a possible web application.  See the
# full configuration options online.
#
#   http://haproxy.1wt.eu/download/1.4/doc/configuration.txt
#
#---------------------------------------------------------------------

#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    # to have these messages end up in /var/log/haproxy.log you will
    # need to:
    #
    # 1) configure syslog to accept network log events.  This is done
    #    by adding the '-r' option to the SYSLOGD_OPTIONS in
    #    /etc/sysconfig/syslog
    #
    # 2) configure local2 events to go to the /var/log/haproxy.log
    #   file. A line like the following can be added to
    #   /etc/sysconfig/syslog
    #
    #    local2.*                       /var/log/haproxy.log
    #
    log         127.0.0.1 local2

    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon

    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000

#---------------------------------------------------------------------
# main frontend which proxys to the backends
#---------------------------------------------------------------------
frontend  main *:5000
    acl url_static       path_beg       -i /static /images /javascript /stylesheets
    acl url_static       path_end       -i .jpg .gif .png .css .js

    use_backend static          if url_static
    default_backend             app

#---------------------------------------------------------------------
# static backend for serving up images, stylesheets and such
#---------------------------------------------------------------------
backend static
    balance     roundrobin
    server      static 127.0.0.1:4331 check

#---------------------------------------------------------------------
# round robin balancing between the various backends
#---------------------------------------------------------------------
backend app
    balance     roundrobin
    server  app1 127.0.0.1:5001 check
    server  app2 127.0.0.1:5002 check
    server  app3 127.0.0.1:5003 check
    server  app4 127.0.0.1:5004 check
```

### Пример haproxy конфига для обычного проброса tcp
```
global
        user haproxy
        group haproxy
        daemon
        maxconn 4096
defaults
        mode    tcp
        balance leastconn
        timeout client      30000ms
        timeout server      30000ms
        timeout connect      3000ms
        retries 3
frontend sbp_in_server
        bind 0.0.0.0:4443
        default_backend sbp_api_in
backend sbp_api_in
        server srv1 172.16.3.224:9998 maxconn 2048
frontend sbp_out_server
        bind 0.0.0.0:14888
        default_backend sbp_api_out
backend sbp_api_out
        server srv1 185.170.1.104:443 maxconn 2048
        server srv2 185.170.1.105:443 maxconn 2048
```

### Пример haproxy конфига для сертификационного проброса tcp
```
global
        user haproxy
        group haproxy
        daemon
        maxconn 4096
defaults
        mode    tcp
        balance leastconn
        timeout client      30000ms
        timeout server      30000ms
        timeout connect      3000ms
        retries 3
frontend sbp_in_server
        bind 0.0.0.0:4443
        default_backend sbp_api_in
backend sbp_api_in
        server srv1 172.16.3.224:9998 maxconn 2048
frontend sbp_out_server_url_prefixes
        bind 0.0.0.0:14888
        default_backend sbp_api_out_url_prefixes
backend sbp_api_out_url_prefixes
        server srv1 sbp-cert1.cbrpay.ru:443 maxconn 2048
        server srv2 sbp-cert2.cbrpay.ru:443 maxconn 2048
frontend sbp_out_server_dict_url_prefixes
        bind 0.0.0.0:14889
        default_backend sbp_api_out_dict_url_prefixes
backend sbp_api_out_dict_url_prefixes
        server srv1 dict.sbp-cert1.cbrpay.ru:443 maxconn 2048
        server srv2 dict.sbp-cert2.cbrpay.ru:443 maxconn 2048
frontend sbp_out_server_report_url_prefixes
        bind 0.0.0.0:14890
        default_backend sbp_api_out_report_url_prefixes
backend sbp_api_out_report_url_prefixes
        server srv1 report.sbp-cert1.cbrpay.ru:443 maxconn 2048
        server srv2 report.sbp-cert2.cbrpay.ru:443 maxconn 2048
```